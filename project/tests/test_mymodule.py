from myCode import mymodule
import unittest
import unittest.mock

class RmTestCase(unittest.TestCase):

	@unittest.mock.patch('myCode.mymodule.oswrapper')
	def test_1asuperAdvanceBehaviorDeleteFile(self, mock_os):
		mymodule.superAdvancedBehaviorDeleteFile('patate.txt')
#		mymodule.superAdvancedBehaviorDeleteFile('toto.txt')
		mock_os.removeFile.assert_called_with("patate.txt")

#	def test_1bsuperAdvancedBehaviorDeleteFile(self):
#		mymodule.superAdvancedBehaviorDeleteFile('patate.txt')


#	def test_2asuperAdvancedBehaviorDeleteFile(self):
#		mymodule.superAdvancedBehaviorDiskSpace()


#	@unittest.mock.patch('myCode.mymodule.oswrapper')
#	def test_2bsuperAdvancedBehaviorDeleteFile(self):
#		mymodule.superAdvancedBehaviorDiskSpace()
#		mock_os.removeFile.assert_not_called()
